# Web Tester Module

A web site testing module following the requirements:

1.	Reads a list of web pages (HTTP URLs) and corresponding page content requirements from a configuration file.
2.	Periodically makes an HTTP request to each page.
3.	Verifies that the page content received from the server matches the content requirements.
4.	Measures the time it took for the web server to complete the whole request.
5.	Writes a log file that shows the progress of the periodic checks.
6.	(OPTIONAL) Implement a single-page HTTP server interface in the same process that shows (HTML) each monitored web site and their current (last check) status.



## Module Details:

* The “content requirement” can for example be just a simple string that must be included in the response received from the server, e.g. one rule might be that the page at the URL [http://www.foobar.com/login](http://www.foobar.com/login) must contain the text “Please login:”.
* The checking period must be configurable via a command-line option or by a setting in the configuration file.
* The log file must contain the checked URLs, their status and the response times.
* The program must distinguish between connection level problems (e.g. the web site is down) and content problems (e.g. the content requirements were not fulfilled).



## Software Requirements

* Python 2.7.2 (not tested in other versions)
* urllib2 Python library (standard in 2.7.2)
* BeautifulSoup library (HTML parsing), MIT license, available at [http://www.crummy.com/software/BeautifulSoup](http://www.crummy.com/software/BeautifulSoup)
* Numpy for loading text files for user config and sites list in an easy way
* Unittest Python module for unit-testing, if required



## Usage and Syntax

The module requires a list of websites and their requirements in a text file (default name: list_sites.txt, but configurable). The syntax of this file is:

> http://www.google.com|{'txt': {'isIn': ['String1', 'String2'], 'isNotIn': ['String3']}, 'img': {'isIn': ['myImage.gif']}}

with one site and set of requirements per line. The separator between the website and the requirements dictionary is a pipe, and there should be no spaces around it. The syntax of the requirements is JSON, interpreted as a Python dictionary.
The allowed parts of the webpage to be checked are `txt`, `img`, `link` and `js`. The rules to be checked are only the two explicitly named `isIn` and `isNotIn`. These are currently hard-coded in the main code.

The module is self-standing and will run until it catches an interrupt from the user (Control+C). No other exception should stop it.

Some parameters of the module are configurable via the `config.json` file. This is also in a JSON format, and all the parameters available in it are already set. The main code has default values for all these parameters, in case not all of them are supplied in the config file.

The output log file contains, per line, the website url, the http return code, the status of the requirements met for the website, and the timing in milliseconds. The http return code is from the [RFC2616](http://www.w3.org/Protocols/rfc2616
) with the additional code 0 in case there is no internet connection. The content status is either False, True or None, depending on whether the requirements are not met, met, or None if the internet connection is not available.

If the module is started in debug mode, i.e.

> python web_tester.py

there is some output to the command line. To suppress it, run with the `-O` flag.

The code is properly commented, and should be self-explanatory. There is a beginning of a unittest file for it, but I did not find enough time to finish it, and preferred writing the documentation correctly in the time I had. The `test_web_tester.py` file runs, but does not test much.


## Required Improvements

* The JSON structure verification is only that of the json.load command for now. Need a built-in syntax check (ideally with a configurable dictionary of keywords somewhere, against which the check is run).
* Ideally, instead of handling exceptions the way I did (which complicates the code, IMO), the module should be run in an hypervisor, as it is supposed to run all the time, with some rules as to how to handle the log file when the module crashes.
* The content matcher is very basic and could use more complex rules, if needed. The json structure for the rules allows this without too much changes in the code.
* The content matcher should have a rule checker, to avoid accepting contradictory rules, for example.
* Some type checking should be done, in the isInData function, for example.
* Timing currently is only for the time it takes to make a HTTP connection to the website. This could be improved to add other timings, such as the time for the actual DNS lookup, the actual connection time, the time it took to be served the page, and such.
* There is a variable called allMetRules in the code, which contains a dictionary of all the outcomes of the rules checking. It could be used in an interface to show which rules have failed, instead of just saying "False" if any of the rules fails.
* Add the optional web server that was mentioned in the requirements for the live monitoring of the status.



# Design Question
> Assuming we wanted to simultaneously monitor the connectivity (and latencies) from multiple geographically distributed locations and collect all the data to a single report that always reflects the current status across all locations. 
> Describe how the design would be different.
> How would you transfer the data? Security considerations?

I will make the assumption in the following that there is a central location (can be one of the sites or not), and a set of nodes geographically distributed. The problem is a bit different if we do not want any central location at all, and that all nodes have to be "in sync" all the time...

I think that for the part of the connection testing at one of these sites, the only real design differences would be:

* How to access the file list? Is it centralized or distributed? Same goes for the config file. Either we have such files locally at each site, or they are accessed (and monitored for changes, e.g.) from a server and get fetched/updated.

* If output files are the preferred way to go (as opposed to some direct communication means between central servers and remote locations), then one output file per website makes sense, at each location, with time stamps and location code written in every line of the log file. The server can then periodically fetch the output files from all locations (this will probably require some lock on the files to make sure data is finished being written), merge them per website and sort them per time stamp or per location and then per time stamp. The display interface only needs to read these files, then.

* If there is some means of more instant communication between the nodes and the central location, then the central server can be the one waiting for the incoming feed from the nodes (which might not need to store the logs, except for safety) and directly write to the correct website log file. This also requires some locking on the files.

* For the matter of transferring the data, I will first consider the case of the files transfer. A periodical one-way (only reading the nodes from the central server) rsync over ssh from the server would do, probably. If the log files are appropriately named at the nodes, it should be ok.
* If we have some instant communication, asymmetric encryption, for example using PGP, should do. If the data is very sensitive, then higher levels of encryptions, maybe (ECC, e.g. but I am not familiar with these).
