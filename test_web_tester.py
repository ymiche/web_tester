import unittest
from mock import *

import numpy as np

from web_tester import readConfig, readListSites

class TestSeparateFunctions(unittest.TestCase):

    def setUp(self):
       self.fileNameListSites = './list_sites.txt'
       self.sitesList, self.reqList = np.genfromtxt(self.fileNameListSites, 
            dtype = None, delimiter = "|", unpack = True)
       self.sitesList = self.sitesList.tolist()
       self.reqList = self.reqList.tolist()

    def test_readConfig(self):
        # TODO More testing on that function, for parameters
        # Calling with a fake file name
        self.assertRaises(Exception, readConfig, 'FakeFileName')

    def test_readListSites(self):
        # Calling with a fake file name
        self.assertRaises(Exception, readListSites, 'FakeFileName')
        # Calling with something that's not a string
        self.assertRaises(Exception, readListSites, dict())
        # Calling with the right file name and checking for urls and content
        # loading
        sitesList, reqList = readListSites(self.fileNameListSites)
        self.assertListEqual(sitesList, self.sitesList)
        self.assertListEqual(reqList, self.reqList)

#    def test_makeRequest(self):
#        with self.assertRaises(Exception):
#            makeRequest(self.url, self.content)
#
#    def test_matchContent(self):
#        with self.assertRaises(Exception):
#            matchContent(self. , self.content)
#
#    def test_writeLog(self):
#        with self.assertRaises(Exception):
#            writeLog(self.logFileName, self.status)

if __name__ == "__main__":
    unittest.main()
