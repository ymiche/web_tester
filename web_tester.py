"""
    web_tester: A simple web tester, with per site rules.

    The module when it is run as is, reads a config file from the user, a list
    of web sites and their associated rules from another file, and periodically
    checks for the connection and the rules to each website. The output of the
    check is written in a log file, and displayed in the command line, if the
    module is ran in debug mode (i.e. without the -O flag).

    The connection status uses the RFC2616 for HTTP return codes, with an
    additional one for the case of no internet connection.

    This module requires:
        - Python 2.7.2+ (not tested in Python 3 nor lower than 2.7.2)
        - urllib2
        - BeautifulSoup 4 (MIT license), see README.md
        - Numpy
        - Unittest for unit-testing, if needed
        - A list of website and their requirements (see README.md for syntax)
        - (Optional) A config file with some parameters, named 'config.json',
          in the same directory and in a JSON format (see README.md for syntax)
"""

# The library for opening URLs
import urllib2
# Parses the config file from the user
import json
# For timings
import time
# For HTML parsing
from bs4 import BeautifulSoup

import ast
import numpy as np
import inspect
import sys


# Global Variables. Empty/Null as default, get filled with the provided config
# ones by the user if a config file is given. Otherwise, affects default
# values.
parameters = {
    "checkingPeriod": 10,
    "connectionTimeout": 10,
    "logFileName": "logfile.log",
    "fileNameListSites": "list_sites.txt"
}


# Read the config file, if any
def readConfig(configFileName):
    """
    Reads the config file and uses default values for
    required parameters if none given by the user.
    Input:
        - configFileName: string, the file name/path of the config parameters
    """
    with open(configFileName, 'r') as configFileHandle:
        configParameters = json.load(configFileHandle)

    for key in configParameters.keys():
        parameters[key] = configParameters[key]


# Read the list of web pages and the requirements for each page
def readListSites(fileNameListSites):
    """
    Reads the given list of web sites, and the requirement for each page
    and puts them in a format used in the rest of the module.
    Input:
        - fileNameListSites: string, the file name/path of the list of sites
          and requirements
    Outputs:
        - sitesList: list, the list of the sites read from the file
        - reqList: list, the associated requirements (in dicts)
    """
    # TODO Better handling of the URLs and of the content reqs.
    sitesList, reqList = np.genfromtxt(fileNameListSites, dtype=None,
                                       delimiter="|", unpack=True)
    sitesList = sitesList.tolist()
    reqList = reqList.tolist()

    return sitesList, reqList


# Check for internet connection
def connectionAccess():
    """
    This is taken from a stackoverflow idea. The ip is that of google and it's
    in ip address form to avoid a possibly long DNS lookup.
    This only works well if that Google server does not crash or if we don't
    get denied access for too many connections, though...
    """
    try:
        urllib2.urlopen('http://109.105.109.251', timeout=2)
        return True
    except urllib2.URLError:
        pass
    return False


# Make a http request to the server to check
def makeRequest(url, reqList):
    """
    Make the request to the given website url and checks for
    the content, if the connection succeeds and the content is
    not empty.
    Inputs:
        - url: string, the url of the website to open
        - reqList: dict, the dictionary of the requirements the page has to
          meet. See README.md for syntax.
    Outputs:
        - connectionStatus: int, uses RFC2616 for HTTP codes, and 0 (not a
          RFC2616 code) for no internet connection
        - contentStatus: boolean or 'None', True if all requirements for the
          page are met, False if one or more are not met, and 'None' if no
          internet connection
        - timings: float (.3f), time in ms for the HTTP request to be answered
    """
    # TODO Improve the timing method and add other timings for the connection
    connectionStatus = 0
    contentStatus = 'None'
    timings = 0.0

    if connectionAccess():
        try:
            urlHandle = urllib2.urlopen(url,
                                        timeout=parameters['connectionTimeout'])
            start = time.clock()
            connectionStatus = urlHandle.getcode()
            # Get the execution time for that last command in microseconds
            timings = (time.clock() - start)*1000

            # Verify that the content matches what was expected
            contentStatus, allMetRules = matchContent(reqList, urlHandle)

        except urllib2.URLError:
            print "Invalid URL in list of websites: %s" % url

    return connectionStatus, contentStatus, timings


# Verify that the page matches the content requirements
def isInData(toFindString, tagList):
    """
    Looks into the tagList produced by Beautiful Soup to check for the
    presence of the given toFindString.
    Inputs:
        - toFindString: string, the string to look for in the list of
          tags+text
        - tagList: the list of tags+text
    """
    # TODO Check for data types
    for element in tagList:
        if toFindString in unicode(element):
            return True
    return False


def checkContent(rules, data):
    """
    Checks that the rules given for the contentType are met for the given data.
    Returns a list of boolean values for each rule given.
    Inputs:
        - rules: dict, contains the rules to meet for one type of content only
        - data: tagList (beautiful soup object), contains the textual content
          of the tags that fit this type of content
    Outputs:
        - metRules: dict, same structure as rules, contains booleans for every
                    rule
    """
    metRules = []
    for rule in rules.keys():
        if rule == 'isIn':
            for element in rules[rule]:
                if isInData(element, data):
                    metRules.append(True)
                else:
                    metRules.append(False)
        elif rule == 'isNotIn':
            for element in rules[rule]:
                if isInData(element, data):
                    metRules.append(False)
                else:
                    metRules.append(True)
        else:
            raise Exception('Invalid rule.')

    return metRules


def matchContent(reqList, urlHandle):
    """
    Check if the content of the page matches the requirements of the content
    variable.
    Inputs:
        - reqList: dict, containing a set of rules for each type of
                   content. See README.md for syntax
        - urlHandle: file-like object from urllib2.open, handle to the
                     url data and HTTP code returned
    Outputs:
        - contentStatus: boolean, True if all requirements are met, False
          otherwise
        - allMetRules: dict, contains the outcome of all the rules checking.
                       Unused in the current code.
    """
    # TODO Make a content matcher that does something else than just text
    # matching
    # TODO Use the allMetRules variable for something

    # Get the data of the page from the URL handle
    allHTMLPage = urlHandle.read()
    # Use the Beautiful Soup parser to parse the HTML for our needs
    soup = BeautifulSoup(allHTMLPage)
    # Initialize an empty dict holding all the results of the tests
    allMetRules = {}

    # The dictionary of the verified HTML tags for the respective content
    # matching
    contentTypes = {'txt': ['title', 'p', 'h1', 'h2'],
                    'img': ['a', 'img'],
                    'link': ['a'],
                    'js': ['script']}

    for contentType in reqList.keys():
        if contentType not in contentTypes.keys():
            raise Exception('Invalid content type in the list of sites file.')
        # Get the HTML content that matches the tags we want
        contentTypeRelevantData = soup.find_all(contentTypes[contentType])
        # And check if the content requirements are met for it
        allMetRules[contentType] = checkContent(reqList[contentType],
                                                contentTypeRelevantData)

    contentStatus = True
    for key in allMetRules.keys():
        for element in allMetRules[key]:
            if element is False:
                contentStatus = False

    return contentStatus, allMetRules


# Write a log file which shows the progress of the periodic checks
def writeLog(logFileName, stringToWrite):
    """
    Writes the results of the requests to the logfile.
    Inputs:
        - logFileName: string, path/name of the log file to use
        - stringToWrite: string, content to be written in the log
    """
    logFileNameHandle = open(logFileName, 'a+')
    logFileNameHandle.write(stringToWrite)
    logFileNameHandle.close()


def main():
    """
    Default behavior if the module gets called directly:
        - Read config file, if available and load its parameters
        - Read the sites and requirements list, quit if we can't read it
        - Run forever (until keyboard exception), and check the site,
          the requirements and the timing
        - Write the results to the log file
    """
    try:
        # Read the config file, first
        readConfig('./config.json')
    except IOError:
        # We can still continue if it is not there...
        print "Configuration file could not be loaded, check file name."
        print "Continuing with default values...\n"

    try:
        # Read the list of sites and requirements
        sitesList, reqList = readListSites(parameters['fileNameListSites'])
    except:
        # This time we cannot continue if we don't have a valid list
        print "Sites and Requirements file invalid, check README.md."
        sys.exit()

    while True:
        try:
            for site, reqs in zip(sitesList, reqList):
                connectionStatus,\
                    contentStatus,\
                    timings = makeRequest(site, ast.literal_eval(reqs))

                myOutput = str(site) + ' ' + str(connectionStatus) + \
                    ' ' + str(contentStatus) + ' ' + \
                    str(round(timings, 4)) + '\n'

                writeLog(parameters['logFileName'], myOutput)
                if __debug__:
                    print myOutput
            time.sleep(parameters['checkingPeriod'])
        except (KeyboardInterrupt, SystemExit):
            print "Keyboard Interruption or System Exit, quitting...\n"
            sys.exit()
        except Exception as myException:
            print "Caught an error in %s: %s" % (inspect.stack()[0][3],
                                                 myException)
            print "Continuing...\n"
            time.sleep(parameters['checkingPeriod'])


if __name__ == "__main__":
    main()
